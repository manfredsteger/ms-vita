---
title: 'Soziale Fertigkeiten'
date: '13:34 10/28/2017'
taxonomy:
    category: left
column1_name: Desktop
column1_animation: fadeIn
column2_name: Kenntnisse
column2_animation: fadeIn
column1:
    -
        name: Kommunikation
        level: 8
    -
        name: 'Reflektierter Umgang'
        level: 8
    -
        name: Empathie
        level: 8
    -
        name: Teamfähigkeit
        level: 6
    -
        name: Kritikfähigkeit
        level: 5
    -
        name: 'Analytische Fertigkeiten'
        level: 7
    -
        name: 'Lebenslanges Lernen'
        level: 8
column2:
    -
        name: 'Prozess- und Teamförderung'
    -
        name: 'Selbstbewusstsein & -wahrnehmung'
    -
        name: Teambuilding
    -
        name: Allrounder
    -
        name: 'Evaluation & Reflexion'
    -
        name: Bedarfsermittlung
    -
        name: 'Neugierde & Offenheit'
---
