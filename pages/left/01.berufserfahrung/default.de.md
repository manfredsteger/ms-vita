---
title: Berufserfahrung
date: '13:35 11/21/2018'
taxonomy:
    category:
        - left
template: education
education:
    -
        date: '2020 – heute'
        topic: 'Wissenschaftlicher Referent und Webentwickler'
        school: 'Zentrum für Medienkompetenz in der Frühpädagogik, Amberg'
    -
        date: '2018 – 2020'
        topic: 'Projektleiter BRiNO - Bildungsregion Nordoberpfalz'
        school: 'Ostbayerische Technische Hochschule, Amberg-Weiden'
    -
        date: '2016 – 2018'
        topic: 'Wissenschaftlicher Mitarbeiter im Projekt: Synergien für Lehren und Lernen durch OER'
        school: 'Univerität Hamburg'
    -
        date: '2016 – 2017'
        topic: 'Frontend Entwickler im Bereich Design, Konzeption und Implementierung für das staatlich geförderte Projekt: Hamburg Open Online University'
        school: 'Multimedia Kontor, Hamburg'
    -
        date: '2013 – 2016'
        topic: 'Studentischer Angestellter des Medienzentrums der Erziehungswissenschaft als Leiter der offenen Werkstatt und E-Coach in der Fakultät für Erziehungswissenschaft'
        school: 'Universität Hamburg'
    -
        date: '2012 – 2013'
        topic: 'Studentischer Angestellter für das Institut Aerodynamik Department Fahrzeugtechnik und Flugzeugbau als Webdesigner und Grafiker'
        school: 'Hochschule für Angewandte Wissenschaften, Hamburg'
    -
        date: '2011 – 2013'
        topic: 'Studentischer Angestellter für das Institut für Technik, Arbeitsprozesse und Berufliche Bildung als Konzeptleiter für das Corporate Design und Webpräsenz'
        school: 'Technische Hochschule, Hamburg-Harburg'
    -
        date: '2003 – 2017'
        topic: '15 Jahre Berufserfahrung als selbstständiger Mediengestalter im Bereich Digital- und Printmedien, mit der Spezialisierung auf Konzeptentwicklung und Content Management'
        school: '- always beta -'
---

