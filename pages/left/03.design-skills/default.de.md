---
title: 'Design Fertigkeiten'
date: '13:34 10/29/2017'
column1_name: Desktop
column1_animation: fadeIn
column2_name: Kenntnisse
column2_animation: fadeIn
column1:
    -
        name: 'Adobe Photoshop'
        level: 7
    -
        name: 'Adobe Illustrator'
        level: 2
    -
        name: 'Adobe InDesign'
        level: 8
    -
        name: 'Adobe Acrobat'
        level: 7
    -
        name: 'Desktop Publishing'
        level: 7
    -
        name: Crossmedia
        level: 6
    -
        name: 'Barrierefreies Design'
        level: 7
    -
        name: Fotografie
        level: 7
column2:
    -
        name: Bildbearbeitung
    -
        name: Vektorisierung
    -
        name: 'Buchproduktion & Design'
    -
        name: 'Accessibility, Preflight'
    -
        name: 'Bücher, Flyer & Poster'
    -
        name: 'Desktop & Mobile'
    -
        name: 'PDFs, Mobi & Websites'
    -
        name: 'Konzerte, Landschaft & Produkte'
taxonomy:
    category: left
---

