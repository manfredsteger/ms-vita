---
title: 'Web Fertigkeiten'
date: '13:34 10/30/2017'
taxonomy:
    category:
        - left
template: skills
column1_name: Desktop
column1_animation: fadeIn
column2_name: Kenntnisse
column2_animation: fadeIn
column1:
    -
        name: Atom
        level: 5
    -
        name: Terminal
        level: 5
    -
        name: Docker
        level: 3
    -
        name: Webserver
        level: 7
    -
        name: 'HTML & CSS'
        level: 6
    -
        name: 'CSS Frameworks'
        level: 7
    -
        name: CMS
        level: 7
    -
        name: LMS
        level: 6
    -
        name: Internet
        level: 7
column2:
    -
        name: 'Coding & Hacking'
    -
        name: Webentwicklung
    -
        name: Systemintegration
    -
        name: 'CMS & LMS'
    -
        name: 'Barrierefreiheit, UX- & UI'
    -
        name: 'Bootstrap & Semantic UI'
    -
        name: 'Wordpress, Joomla & Grav'
    -
        name: 'Moodle & CommSy'
    -
        name: 'SEO & DOM'
---
