---
title: 'Hobbies & Interessen'
date: '13:34 10/26/2014'
interests:
    -
        icon: camera
        activity: Fotografie
        animation: fadeIn
    -
        icon: universal-access
        activity: Barrierefreiheit
        animation: fadeIn
    -
        icon: social-apple
        activity: Kochen
        animation: fadeIn
    -
        icon: music
        activity: 'Live Musik'
        animation: fadeIn
    -
        icon: map
        activity: Fahrrad
        animation: fadeIn
    -
        icon: skull
        activity: 'Raspberry Pi'
        animation: fadeIn
    -
        icon: ticket
        activity: Kultur
        animation: fadeIn
    -
        icon: projection-screen
        activity: Bildung
        animation: fadeIn
taxonomy:
    category: left
---
