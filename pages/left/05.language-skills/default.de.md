---
title: 'Sprach Kenntnisse'
date: '13:34 10/27/2017'
taxonomy:
    category:
        - left
template: langskills
languages:
    -
        name: Bayerisch
        level_name: Muttersprache
        level: 100
    -
        name: Deutsch
        level_name: Fortgeschritten
        level: 90
        animation: bounceIn
    -
        name: Englisch
        level_name: Basal
        level: 40
        animation: bounceIn
---
