---
title: Erstausbildung
date: '13:34 09/29/2017'
taxonomy:
    category:
        - right
template: education
education:
    -
        date: '2016'
        topic: 'Master of Education (M.Ed.) Lehramt an Beruflichen Schulen'
        school: 'Univerität Hamburg'
    -
        date: '2014'
        topic: 'Bachelor of Science (B.SC.) Lehramt an Beruflichen Schulen'
        school: 'Technische Universität, Hamburg-Harburg'
    -
        date: '2009'
        topic: 'Allgemeine Hochschulreife'
        school: 'Berufsoberschule, Amberg'
    -
        date: '2005'
        topic: 'Ausbildung zum Mediengestalter Digital- und Printmedien'
        school: 'Buero Wilhelm, Amberg'
---

