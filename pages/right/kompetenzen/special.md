---
title: Kompetenzen
date: 13:33 09/23/2017
specialities:
    - icon: lightbulb
      text: Arbeitsprozesse
      animation: fadeInDown
    - icon: page-multiple
      text: Content Management
      animation: fadeInUp
    - icon: results
      text: Web Design
      animation: fadeInLeft
taxonomy:
    category: right
---

##### Fachkompetenz

Im beruflichen wie persönlichen befasse ich mich eingehend mit der Planung, Gestaltung und Durchführung von Medienprodukte aller Art. Mein Schwerpunkt liegt im Fachgebiet der Online-Medien, darunter:

**Content Management Systeme (WordPress/Joomla/Grav)**

![Abbildung der Internetseite Energieforschungsverbund.hamburg – Design und Umsetzung von Manfred Steger](portfolio-efh.png)

---

**Barrierefreie Medien (Accessibility Checkup/barrierefreie PDF's)**

![Abbildung des Workshop Programms der Medienzentrums der Universität Hamburg – Masterdatei und PDF-Accessibility Checkup von Manfred Steger](portfolio-pdf-barrierefrei.png)

---

**Suchmaschinenoptimierung**

![Abbildung des Grav Dashboards mit Anzeige von Backups/Aktualisierungen und der Benutzerstatistik von manfred-steger.de](portfolio-seo.png)

---

**Webdesign**

![Abbildung der Internetseite von Aida Sikira Pianistin aus Hamburg – Design, Webdesign und Webentwicklung von Manfred Steger](portfolio-webdesign-aida.png)

---

**Printdesign**

![Abbildung einer Doppelseite der LEO Umfeldstudie von 2015 – Layout, Design, Infografiken und Druckvorbereitung von Manfred Steger](portfolio-broschure.png)

---

**Informationsgrafiken**

![Abbildung der Infografik zur Erklärung der psychologischen und visuellen Darstellung der Leo-Umfeldstudie mit semantischen Ankern (Icons) von Ralf Appelt und Manfred Steger](portfolio-infografik.jpg)

---

Ich bemühe mich in meinem Fachgebiet der Mediengestaltung immer auf dem neuesten Stand zu bleiben, ein Beispiel hierfür ist der **[Blog](http://www.manfred-steger.de)** auf meiner Internetseite, dort befasse ich mich mit neuen Techniken, Arbeitsweisen und zukunftsweisenden Technologien rund um das Gebiet aktuelle Medien.

##### Sozialkompetenz

Während meiner **15 Jahre Berufserfahrung als selbstständiger Mediengestalter**, Auszubildender und Angestellter habe ich unterschiedliche Fähigkeiten und Kenntnisse im sozialen Umgang hinzugewonnen. Bei größeren Projekten wie z.B. dem **HCAT - Hamburg Centre of Aviation Training** wurden Team-, Kommunikation-, und Kompromissfähigkeiten gefordert und gefördert.

Bei **Fotoexperience** habe ich zusammen mit Olaf Malzahn als **freier Fotojournalist** gearbeitet, im Zweierteam wird die Eigenverantwortung stark unter Beweis gestellt, diese hat sich in der dreijährigen Zusammenarbeit zum Positiven weiterentwickelt.

**Kundenorientierung** spielt bei der Selbstständigkeit eine tragende Rolle, diese Fähigkeit wuchs über die Jahre heran, ich biete viele Dienstleistungen für unterschiedlichste Kunden an: Logos, Internetseiten, Content Management Systeme, Fotografien sowie Bildbearbeitung, Suchmaschinenoptimierung, Schulungen wie Kurse und Beratung bei der Medienplanung und -umsetzung.

Nicht nur im Arbeitsalltag sind **soziale Kompetenzen** gefragt, als leidenschaftlicher Koch im CC – Club kochender Männer ist bei der Planung, Zubereitung und Reflexiongesprächen von mehrgängigen Menüs ein Maß an Selbstdisziplin und Kritikfähigkeit erforderlich.

##### Handlungskompetenz

###### Internetauftritte

- [Manfred's Tech&Edu Blog](https://www.manfred-steger.de)
- [Schmankerl.space](https://www.schmankerl.space)
- [Markus Deimann](http://markusdeimann.de/)
- [Energieforschungsverbund Hamburg](http://www.energieforschungsverbund.hamburg)
- [Hanseatischer Fechtclub Lübeck](http://www.hfcl.de)

###### Workshops & Fortbildungen

- Raspberry Pi – Get-Together
- Raspberry Pi – Multimediacenter
- Raspberry Pi – Die eigene Cloud mit OwnCloud
- Raspberry Pi – Datenmanagement und Synchronisation
- Markdown: Eine Formatvorlage für alle Textverarbeitungsprogramme
- Print Design mit InDesign
- Einführung, Teamkommunikation und -kollaboration in GitLab
- Projektmanagement mit GitLab im Hochschulkontext
- Responsive Websites mit Bootstrap und Semantic UI
- Linux für Einsteiger
- Kollaboratives Arbeiten (Anfänger)
- Photoshop: Einsatz im Medienalltag (Anfänger)
- Photoshop: Panoramas - 180° – 360° Stiching (Anfänger)
- Photoshop/InDesign: Internetseiten mit Vektoren und Pixel gestalten (Fortgeschrittene)
- CSS: Vom Layout zur W3C kompatiblen Internetseite (Anfänger / Fortgeschrittene)
- CMS: Joomla: Installieren, konfigurieren, administrieren (Anfänger)
- CMS: Joomla: Das eigene Template (Fortgeschrittene / Experten)
- CMS: Wordpress installieren, konfigurieren und administrieren
- CMS: WordPress – Erste Schritte ins System
- CMS: Datenbanklose Content Management System – Hands-On Grav Flat File System
- MAC&PC: Arbeiten am Hybridsystem (Anfänger)
- InDesign: Praktisch-gestalterisch zur professionellen Hausarbeit (Anfänger)
- SEO (Suchmaschinenoptimierung): Googelst du noch oder suchst du schon? (Anfänger)
- Cloud Systeme: Kollaboratives Arbeiten mit Open Source Services (Anfänger)
- Fotografie / Photoshop / Indesign: Triptychon und mehrteilige Bilder (Fortgeschrittene)
- LaTeX: Installieren, konfigurieren, wissenschaftlich schreiben (Anfänger/Fortgeschrittene)
- Studieren und arbeiten mit dem MAC (Anfänger)
- SmartBoard Schulungen (Anfänger)

###### Veröffentlichungen

**(Zur Zeit wegen Plattformarbeiten nicht erreichbar)**

- Bericht: "Ni hao" und Willkommen beim Chinesischen Nationalcircus
- Bericht: Mystisches Lübeck - Die Rückkehr der Shaolin
- Bericht: Ana Popovic - Jimi Hendrix mit Hüftschwung
- Bericht: Olaf Schubert - Seine Kämpfe, unsere Krämpfe
- Bericht: Paul Panzer - Vom Heimatabend Deluxe in den Freizeitstress
- Bericht: 21 Jahre und kein bisschen leise - Wacken Open Air 2010
- Bericht: WM 2010 - Impressionen vom Fan-Fest in der MuK
- Bericht: Die Wise Guys - a capella für die Sinne
- Bericht: Deutschland vs. England - WM-Fanfest am 27. Juni 2010 in der MuK
- Bericht: [Wacken 2010] Tag 1: Wacken, Wackinger und viele Musikfans
- Bericht: [Wacken 2010] Tag 2: Und der Wahnsinn geht weiter
- Bericht: [Wacken 2010] Tag 3: Ein schweißtreibender Endspurt
- Bericht: [Wacken 2010] Alle Wacken wieder – ein Rückblick
- Bericht: Blink 182 - Wiedervereinigt und auf Tour
- Bericht: Ein Abend mit Daughtry - von American Idol zum Rock Newcomer
- Bericht: Hagen Rether: Liebe mit Biss
- Bericht: Rolling Stone Weekender - Der Ü30 Festivalklassiker
- Bericht: Torfrock - Nordrock mit Humor und langer Geschichte
- Bericht: Power of Metal Tour 2011 Markthalle Hamburg
- Bericht: Bülent Ceylan und die "Turbülenzen" in der MuK
- Bericht: 22 Jahre und schon wieder ausverkauft! - Wacken Open Air 2011
- Bericht: Wacken 2011 - Regen über Wacken, wen stört's?
- Bericht: Wacken 2012 - Louder than Hell....
- Bericht: Guano Apes - Live aus der Alsterdorfer Sporthalle
- Bericht: Dockville Festival - Baggern auf der Flussinsel
